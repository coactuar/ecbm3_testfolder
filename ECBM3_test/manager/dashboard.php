<?php
require_once "../controls/sesAdminCheck.php";
require_once "../functions.php";
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dashboard</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/all.min.css">
  <link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
  <nav class="navbar navbar-expand-md bg-light">
    <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="dashboard.php">Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="users.php">Registered Users</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="sessions.php">Webcast Sessions</a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" href="polls.php">Polls</a>
        </li>
        
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="?action=logout">Logout</a>
        </li>
      </ul>

    </div>
  </nav>

  <div class="container-fluid bg-white color-grey">

    <div class="row mt-1 p-2">
      <div class="col-12 col-md-4 offset-md-1">
        <h6>Registered Users</h6>
        <?php
        $members = new Admin();
        $reg_users = $members->getMemberCount();
        echo 'Total Registed Users: ' . $reg_users . '<br>';
        
        ?>
      </div>
      
    </div>
    

    <div class="row mt-3">
      <div class="col-12 col-md-4 offset-md-1 report">
        <h6>Sessions Attendee Count</h6>
        <?php
        $sessions = new Admin();
        $ses_cnt = $sessions->getSessionsCount();
        echo 'Total Sessions: ' . $ses_cnt . '<br>';

        $ses_attcnt = $sessions->getSessionAttendeeCount();
        if (!empty($ses_attcnt)) {
        ?>
          <table class="table table-dark">
            <?php
            foreach ($ses_attcnt as $sess) {
              $session = new Session();
              $ses_title = $session->getWebcastSessionTitle($sess['session_id']);
            ?>
              <tr>
                <td><?php echo $ses_title; ?></td>
                <td><?php echo $sess['cnt']; ?></td>
              </tr>
            <?php
            }
            ?>
          </table>
        <?php
        }
        ?>

      </div>
      <div class="col-12 col-md-4 offset-md-1 report">
        <h6>Sessions Attended</h6>
        <?php
        $sessions = new Admin();
        $ses_att = $sessions->getSessionAttended();
        echo 'Total Session Attendees: ' . $ses_att . '<br>';

        $attendees = $sessions->getSessionAttendees();
        if (!empty($attendees)) {
        ?>
          <table class="table table-dark">
            <?php
            foreach ($attendees as $attendee) {
              $members = new User();
              $user = $members->getMember($attendee['user_id']);
            ?>
              <tr>
                <td><?php echo $user[0]['first_name'] . ' ' . $user[0]['last_name']; ?></td>
                <td><?php echo $attendee['cnt']; ?></td>
              </tr>
            <?php
            }
            ?>
          </table>
        <?php

        }

        ?>

      </div>
    </div>
    
  </div>


  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script>
    $(function() {

    });
  </script>
  <?php
  /*function tosecs($seconds)
  {
    $t = round($seconds);
    return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
  }*/
  ?>
</body>

</html>
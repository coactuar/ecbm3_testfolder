<?php
require_once "controls/sesCheck.php";
$curr_room = 'lobby';
?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1100">
    <title>Lobby :: <?php echo $event_title; ?></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/overlays.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

    <style>
    #player{
        width:100%;
        height:100vh;
    }
    </style>
</head>

<body>
    <div id="main-wrapper">
        <header class="topbar color-grey">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-collapse">
                    <ul class="navbar-nav ml-auto top-right-menu">
                        <!--<li class="nav-item">
                            <div class="live-msg btn btn-danger btn-sm">Event is LIVE in Auditorium 1,2,3</div>
                        </li>-->
                        <li class="nav-item">
                            <?php require_once "attendance.php" ?>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <div class="page-wrapper">
            <div id="main-area" style="display:block;">
                <div id="background-image" class="lobby-bg" style="background-image:url(img/lobby.jpg); background-position:center top; "></div>

                <div id="overlays">
                    <div class="overlay-item lobby-video-area">
                        <div class="exhib-actions">
                            <!--<a class="hall-video" href="video-lobby.php"></a>-->
                            <div id="player"></div>
                        </div>
                    </div>

                    <a class="overlay-item lobby-auditorium-01" href="auditorium-01.php">
                        <div class="indicator d-8"></div>
                    </a>
                    <a class="overlay-item lobby-auditorium-02" href="auditorium-02.php">
                        <div class="indicator d-8"></div>
                    </a>
                    <a class="overlay-item lobby-auditorium-03" href="auditorium-03.php">
                        <div class="indicator d-8"></div>
                    </a>
                    <a class="overlay-item lobby-agenda agenda_open" href="#">
                        <div class="indicator d-6"></div>
                    </a>
                    <a class="overlay-item lobby-resources icon_open" href="#">                    
                        <div class="indicator d-6"></div>
                    </a>
                    <a class="overlay-item lobby-ivr" href="https://forms.office.com/Pages/ResponsePage.aspx?id=Ee2EIDX3SkK6KPnTaSNT6Auu-1MJO9dOrbd7M_SgD4BUMUpRNkI4SlozVzA4NTMxN1BPVEtXQU5WNy4u" target="_blank">                    
                        <div class="indicator d-6"></div>
                    </a>
                    
                     <a class="overlay-item lobby-max hall-res " href="resources/MumferMaxMaximiser.pdf">                    
                        <div class="indicator d-4"></div>
                    </a>
                    <a class="overlay-item lobby-od hall-res" href="resources/MilicalODMaharaja.pdf">                    
                        <div class="indicator d-4"></div>
                    </a>
                    <a class="overlay-item lobby-duba hall-res" href="resources/DubagestKeDabang.pdf">                    
                        <div class="indicator d-4"></div>
                    </a>
                    <a class="overlay-item lobby-fenza hall-res" href="resources/FENZAkeFrontRunnersPoster.pdf">                    
                        <div class="indicator d-4"></div>
                    </a>
                    <a class="overlay-item lobby-fenzacream hall-res" href="resources/FENZACREAMkeChampions.pdf">                    
                        <div class="indicator d-4"></div>
                    </a>

                </div>

                <?php require_once "bottom-navmenu.php" ?>

            </div>

        </div>

    </div>

    <?php require_once "commons.php" ?>
<div id="icon" class="scroll popup-dialog">
    <a class="icon_close popup-close" href="#"><i class="fas fa-times"></i></a>
    <div class="heading">
        <h4>View Integrace ICON</h4>
    </div>
    <div class="popup-content">
        <ul class="list-unstyled">
            <li><i class="fas fa-file-pdf"></i><a class="icon_close orthoicon_open" href="#" title=""><span class="hide-menu"></span>Ortho Divsion Quaterly Icons</a></li>
            <li><i class="fas fa-file-pdf"></i><a class="icon_close gynacicon_open" href="#" title=""><span class="hide-menu"></span>Gynaec Divsion Quaterly Icons</a></li>
        </ul>
    </div>
</div>
<div id="orthoicon" class="scroll popup-dialog">
    <a class="orthoicon_close popup-close" href="#"><i class="fas fa-times"></i></a>
    <div class="heading">
        <h4>Ortho Division Quaterly Icons</h4>
    </div>
    <div class="popup-content">
        <iframe src="resources/OrthoQuaterly.pdf#toolbar=0" frameborder="0" scrolling="no"></iframe>
    </div>
</div>
<div id="gynacicon" class="scroll popup-dialog">
    <a class="gynacicon_close popup-close" href="#"><i class="fas fa-times"></i></a>
    <div class="heading">
        <h4>Gynaec Division Quaterly Icons</h4>
    </div>
    <div class="popup-content">
        <iframe src="resources/GyaneQuarter.pdf#toolbar=0" frameborder="0" scrolling="no"></iframe>
    </div>
</div>
    

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- <script type="text/javascript" src="lightbox/html5lightbox.js"></script> -->
    <script src="js/jquery.popupoverlay.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script data-loc="<?php echo $curr_room; ?>" src="js/site.js?v=2"></script>
    <script data-to="<?php echo $_SESSION['user_id']; ?>" src="js/functions.js"></script>
    <script>
        var player = new Clappr.Player(
        {
            source: "https://d38233lepn6k1s.cloudfront.net/out/v1/c93c714e139f4640939b493d754f7a39/4553131ab95f4d748725f284186e6605/57c32c311a6e4f23bdd4bd4d63d426a1/index.m3u8",      
            parentId: "#player",
            //poster: "img/poster.jpg",
            width: "100%",
            height: "100%",
            loop: true,
            autoplay: true
            
        });
        
        player.play();
        
        $('#icon').popup({
            transition: 'all 0.5s',
            backgroundactive: true
        });
        $('#orthoicon').popup({
            transition: 'all 0.5s',
            backgroundactive: true
        });
        $('#gynacicon').popup({
            transition: 'all 0.5s',
            backgroundactive: true
        });
    </script>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-22"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-22');
</script>


</body>

</html>
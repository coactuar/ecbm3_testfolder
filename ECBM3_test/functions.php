<?php
spl_autoload_register(function($classname){
    $path = '../model/' . strtolower($classname).".php";
    //echo $path.'<br>'; 
    if(file_exists($path)){
        require_once($path);
        //echo "File $path is found.<br>";
    }
    else{
        echo "File $path is not found.";
    }
});

?>
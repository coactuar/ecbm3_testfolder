<?php
require_once "../controls/config.php";
//require_once "../functions.php";

if (!isset($_SESSION["super_user"])) {

  header("location: ./");
  exit;
}
if (isset($_GET['action']) && !empty($_GET['action'])) {
  $action = $_GET['action'];
  if ($action == "logout") {
    unset($_SESSION['super_user']);
    header("location: ./");
    exit;
  }
}


?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Reports</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/all.min.css">
  <link rel="stylesheet" type="text/css" href="../css/styles.css">
  <link rel="stylesheet" type="text/css" href="../css/admin.css">



</head>

<body class="admin">
  <nav class="navbar navbar-expand-md bg-white">
    <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item ">
          <a class="nav-link" href="dashboard.php">Dashboard</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="reports.php">Reports</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Hello, <?php echo $_SESSION["super_user"]; ?>!</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="?action=logout">Logout</a>
        </li>
      </ul>

    </div>
  </nav>

  <div class="container-fluid">
    <div id="superdashboard">
      <div class="row">
        <div class="col-12">
          <h6>Session Attendees</h6>
          <?php
          $query = "SELECT count(DISTINCT user_id) as cnt, tbl_sessions_attendee.session_id FROM `tbl_sessions_attendee`, tbl_sessions where tbl_sessions_attendee.session_id=tbl_sessions.session_id group by tbl_sessions_attendee.session_id order by cnt desc";

          $rs = mysqli_query($link, $query);

          if (mysqli_affected_rows($link) > 0) {
          ?>
            <table class="table table-dark">
              <?php
              while ($data = mysqli_fetch_assoc($rs)) {
                $sql = "select audi_id,session_title, start_time from tbl_sessions where session_id='" . $data['session_id'] . "' limit 1";
                $e = mysqli_query($link, $sql);
                $d = mysqli_fetch_assoc($e);
                $ses_title =  $d['session_title'];
                $ses_time = $d['start_time'];

                $sql = "select audi_name from tbl_auditoriums where audi_id='" . $d['audi_id'] . "' limit 1";
                $e = mysqli_query($link, $sql);
                $d = mysqli_fetch_assoc($e);
                $audi =  $d['audi_name'];
              ?>
                <tr>
                  <td><?php 
                  $date = date_create($ses_time);
                  echo date_format($date, "M d, H:i a");
                   ?></td>
                  <td><?php echo $audi . ' - ' . $ses_title; ?></td>
                  <td><?php echo $data['cnt']; ?></td>
                  <td width="50"><a href="sesatt.php?s=<?php echo $data['session_id']; ?>"><i class="fas fa-download"></i></a></td>
                </tr>
              <?php
              }
              ?>
            </table>
          <?php
          }
          ?>
        </div>
        
        
        
        
        
      </div>


    </div>
  </div>


  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>




</body>

</html>
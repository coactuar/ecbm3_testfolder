<?php
//require_once 'config.php';
require_once 'constants.php';
class Poll
{

    private $ds;

    function __construct()
    {
        $this->ds = new DataSource();
    }

    public function getPoll($pollid)
    {
        $query = "select * from tbl_polls where poll_id = ? limit 1";
        $paramType = 's';
        $paramValue = array(
            $pollid
        );

        $poll = $this->ds->select($query, $paramType, $paramValue);
        return $poll;
    }

    public function isValidPoll($pollid)
    {
        $query = "select * from tbl_polls where poll_id = ? limit 1";
        $paramType = 's';
        $paramValue = array(
            $pollid
        );

        $status = $this->ds->getRecordCount($query, $paramType, $paramValue);
        return $status;
    }

    public function endPoll($pollid, $val)
    {
        $query = "Update tbl_polls set poll_over =? WHERE poll_id=?";
        $paramType = 'ss';
        $paramValue = array(
            $val,
            $pollid
        );

        $this->ds->execute($query, $paramType, $paramValue);
    }
    public function updatePollRes($pollid, $sessionid, $val)
    {
        $query = "Update tbl_polls set show_results ='0' WHERE session_id=?";
        $paramType = 's';
        $paramValue = array(
            $sessionid
        );

        $this->ds->execute($query, $paramType, $paramValue);

        $active = $val;
        $query = "Update tbl_polls set show_results =? where poll_id = ?";
        $paramType = 'ss';
        $paramValue = array(
            $active,
            $pollid
        );

        $this->ds->execute($query, $paramType, $paramValue);
    }
    public function updatePoll($pollid, $sessionid, $val)
    {
        $query = "Update tbl_polls set active ='0' WHERE session_id=?";
        $paramType = 's';
        $paramValue = array(
            $sessionid
        );

        $this->ds->execute($query, $paramType, $paramValue);

        $active = $val;
        $query = "Update tbl_polls set active =? where poll_id = ?";
        $paramType = 'ss';
        $paramValue = array(
            $active,
            $pollid
        );

        $this->ds->execute($query, $paramType, $paramValue);
    }



    public function delPoll($pollid)
    {
        $query = "delete from tbl_pollanswers where poll_id=?";
        $paramType = 's';
        $paramValue = array(
            $pollid
        );

        $this->ds->execute($query, $paramType, $paramValue);

        $query = "delete from tbl_polls where poll_id=?";
        $paramType = 's';
        $paramValue = array(
            $pollid
        );

        $this->ds->execute($query, $paramType, $paramValue);
    }

    public function editPoll($pollid)
    {
        $errors = [];
        $succ = '';
        $opt3 = '';
        $opt4 = '';
        $opt5 = '';
        $sess = $_POST['sessionid'];
        $pollques = $_POST['pollques'];
        $opt1 = $_POST['opt1'];
        $opt2 = $_POST['opt2'];
        if (isset($_POST['opt3'])) {
            $opt3 = $_POST['opt3'];
        }
        if (isset($_POST['opt4'])) {
            $opt4 = $_POST['opt4'];
        }
        if (isset($_POST['opt5'])) {
            $opt5 = $_POST['opt5'];
        }
        $ans = $_POST['corrans'];


        $query = "Update tbl_polls set session_id=?, poll_question=?, poll_opt1=?, poll_opt2=?, poll_opt3=?, poll_opt4=?, poll_opt5=?, correct_ans=? where poll_id=?";
        $paramType = 'sssssssss';
        $paramValue = array(
            $sess,
            $pollques,
            $opt1,
            $opt2,
            $opt3,
            $opt4,
            $opt5,
            $ans,
            $pollid,
        );
        $this->ds->execute($query, $paramType, $paramValue);

        return '1';
    }

    public function addPoll()
    {
        $errors = [];
        $succ = '';

        $sess = '0';
        $pollques = '';
        $opt1 = '';
        $opt2 = '';
        $opt3 = '';
        $opt4 = '';
        $opt5 = '';
        $ans = '0';

        if (($_POST['sessionid'] == '0')) {
            $errors['session'] = 'Select Session';
        }
        if (empty($_POST['pollques'])) {
            $errors['pollques'] = 'Poll Question is required';
        }
        if (empty($_POST['opt1'])) {
            $errors['opt1'] = 'Option 1 is required';
        }
        if (empty($_POST['opt2'])) {
            $errors['opt2'] = 'Option 2 is required';
        }
        if (($_POST['corrans'] == '0')) {
            $errors['ans'] = 'Select Correct Answer';
        }

        $sess = $_POST['sessionid'];
        $pollques = $_POST['pollques'];
        $opt1 = $_POST['opt1'];
        $opt2 = $_POST['opt2'];
        if (isset($_POST['opt3'])) {
            $opt3 = $_POST['opt3'];
        }
        if (isset($_POST['opt4'])) {
            $opt4 = $_POST['opt4'];
        }
        if (isset($_POST['opt5'])) {
            $opt5 = $_POST['opt5'];
        }
        $ans = $_POST['corrans'];


        $pollid = bin2hex(random_bytes(24)); // generate unique token
        $active = 0;
        $poll_over = 0;
        $show_results = 0;

        $query = "Insert into tbl_polls(session_id,poll_id, poll_question, poll_opt1, poll_opt2, poll_opt3, poll_opt4, poll_opt5, correct_ans, active, poll_over, show_results) values(?,?,?,?,?,?,?,?,?,?,?, ?)";
        $paramType = 'sssssssssiii';
        $paramValue = array(
            $sess,
            $pollid,
            $pollques,
            $opt1,
            $opt2,
            $opt3,
            $opt4,
            $opt5,
            $ans,
            $active,
            $poll_over,
            $show_results
        );
        $pollid = $this->ds->insert($query, $paramType, $paramValue);

        return $pollid;
    }

    public function getAnsCount($pollid)
    {
        $query = 'select * from tbl_pollanswers where poll_id = ?';
        $paramType = 's';
        $paramValue = array(
            $pollid
        );

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);
        return $count;
    }

    public function getOptAnsCount($pollid, $opt)
    {
        $query = 'select * from tbl_pollanswers where poll_id = ? and poll_answer =?';
        $paramType = 'ss';
        $paramValue = array(
            $pollid,
            $opt
        );

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);
        return $count;
    }

    public function getPollResults($pollid)
    {
    }

    public function currPoll($sessionid)
    {
        $query = 'select * from tbl_polls where session_id = ? and active ="1"';
        $paramType = 's';
        $paramValue = array(
            $sessionid
        );

        $poll = $this->ds->select($query, $paramType, $paramValue);
        return $poll;
    }

    public function submitResponse($pollid, $userid, $option)
    {
        $poll_time   = date('Y/m/d H:i:s');
        $query = "insert into tbl_pollanswers(poll_id, user_id,poll_answer, poll_at) values(?,?,?,?)";
        $paramType = 'ssss';
        $paramValue = array(
            $pollid,
            $userid,
            $option,
            $poll_time
        );

        $pollOpt = $this->ds->insert($query, $paramType, $paramValue);
        return $pollOpt;
    }

    public function isAnswered($pollid, $userid)
    {
        $query = 'select * from tbl_pollanswers where poll_id = ? and user_id =?';
        $paramType = 'ss';
        $paramValue = array(
            $pollid,
            $userid
        );

        $status = $this->ds->getRecordCount($query, $paramType, $paramValue);
        return $status;
    }

    public function showPollRes($sessionid)
    {
        $query = 'select * from tbl_polls where session_id = ? and active ="0" and show_results="1"';
        $paramType = 's';
        $paramValue = array(
            $sessionid
        );

        $poll = $this->ds->select($query, $paramType, $paramValue);
        return $poll;
    }
}

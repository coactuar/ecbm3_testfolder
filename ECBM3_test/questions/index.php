<?php
require_once "../controls/config.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Admin Login : Live Webcast</title>
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../css/styles.css" rel="stylesheet" type="text/css">

</head>

<body class="admin">
<div class="container-fluid">
    <div class="row mt-5 bg-white color-grey p-4">
        <div class="col-8 col-md-6 offset-2 offset-md-3">
            
                        <h6>Select Session</h6>
                        
                        <h5>Day1</h5>
                        <h6><a href="questions.php?s=dbec35e761986cf707608821242afcad521d65a7579dde3f">Audi 01</a></h6>
                        <h6><a href="questions.php?s=c988caf9ba5f9ea75f575ade92d34053f56a17e9d0950a67">Audi 02</a></h6>
                        <h6><a href="questions.php?s=f85f7e6fad0225d693b5344a24dd321b8f8fcccbf38a4b00">Audi 03</a></h6>
                        <br>
                        <h5>Day2</h5>
                        <h6><a href="questions.php?s=a684990fa103934f9d127266ddbd6554400d1fd994bcb172">Audi 01</a></h6>
                        <h6><a href="questions.php?s=a8cb79713d84c4d3a880b05deabc795552c065b81062d610">Audi 02</a></h6>
                        <h6><a href="questions.php?s=2d672787c82b65b0e9fa93e4317cb29e429d104f113511a5">Audi 03</a></h6>
                        <br>
                        <h5>Day3</h5>
                        <h6><a href="questions.php?s=c019dda9202475eab9dfa480e58b8717a8d9f82e8de16268">Audi 01</a></h6>
                        <h6><a href="questions.php?s=eba5b22b83e820ba4ae1d74bb2cdf1763acfd14092488d8b">Audi 02</a></h6>
                        
                  </div>
                  
                
            </form>
            </div>
        
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script>
$(function(){
  $(document).on('submit', '#login-form', function()
{  
var sess = $('#session').val();
if(sess == 0)
{
    $('#login-message').text('Please select session');
    $('#login-message').addClass('alert-danger');
    return false;
}
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
      
      if(data=="0")
      {
        $('#login-message').text('Invalid login. Please try again.');
        $('#login-message').addClass('alert-danger');
      }
      else if(data =='s')
      {
        window.location.href='questions.php?s='+sess;  
      }
      
  });
  
  return false;
});

});

</script>
</body>
</html>
<?php
require_once "../controls/config.php";
$session_id = $_GET['s'];

$sql = "select session_title from tbl_sessions where session_id='" . $session_id . "'";
$rs = mysqli_query($link, $sql);
$d = mysqli_fetch_assoc($rs);
$title = $d['session_title'];


$sql = "select tbl_session_questions.id, tbl_session_questions.user_id, tbl_session_questions.question,  tbl_session_questions.asked_at, tbl_users.first_name, tbl_users.last_name, tbl_users.emailid, tbl_users.division, tbl_sessions.session_title from tbl_session_questions, tbl_users, tbl_sessions where tbl_session_questions.user_id=tbl_users.userid and tbl_session_questions.session_id=tbl_sessions.session_id and tbl_session_questions.session_id='".$session_id."'";
$rs = mysqli_query($link, $sql);
$data = array();
if (mysqli_affected_rows($link) > 0) {
  $i = 0;
  while ($c = mysqli_fetch_assoc($rs)) {
    $data[$i]['First Name'] = $c['first_name'];
    $data[$i]['Last Name'] = $c['last_name'];
    $data[$i]['E-mail ID'] = $c['emailid'];
    $data[$i]['Division'] = $c['division'];
    $ques = str_replace('<br />' , '  ' ,  nl2br($c['question']));
    $data[$i]['Question'] = $ques;
    $data[$i]['Asked At'] = $c['asked_at'];

    $i++;
  }

  $filename = $title . "_questions.xls";
  header("Content-Type: application/vnd.ms-excel");
  header("Content-Disposition: attachment; filename=\"$filename\"");
  ExportFile($data);
}

function ExportFile($records)
{
  $heading = false;
  if (!empty($records))
    foreach ($records as $row) {
      if (!$heading) {
        // display field/column names as a first row
        echo implode("\t", array_keys($row)) . "\n";
        $heading = true;
      }
      echo implode("\t", array_values($row)) . "\n";
    }
  exit;
}

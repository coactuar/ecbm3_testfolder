"use strict";
var script = $('script[src*=function]'); // or better regexp to get the file name..

var userId = script.attr('data-to');
if (typeof userId === "undefined") {
    userId = '0';
}

var getteamchat;

$(function () {

    $('#auditoriums').popup({
        transition: 'all 0.5s',
    });
    $('#agenda').popup({
        transition: 'all 0.5s',
        backgroundactive: true
    });
    $('#orthoagenda').popup({
        transition: 'all 0.5s',
        backgroundactive: true
    });
    $('#gynagenda').popup({
        transition: 'all 0.5s',
        backgroundactive: true
    });
    
    

    //setInterval(function () { checkfornewchat(userId); }, 20000);
    //checkfornewchat(userId);
    $('#attendees_list').dialog({
        autoOpen: false,
        title: 'Online Attendees',
        position: { my: "center center", at: "center center" }
    });

    $('#my_briefcase').dialog({
        autoOpen: false,
        title: 'My Briefcase',
        position: { my: "center center", at: "center center" }
    });
    $('#talktous').dialog({
        dialogClass: "talktoteam",
        autoOpen: false,
        title: 'Talk to Team Integrace',
        position: { my: "center center", at: "center center" },
        beforeClose: function (event, ui) { clearInterval(getteamchat); }
    });



    $(document).on('click', '#show_attendees', function () {
        getOnlineAttendees('');
        $('#attendees_list').dialog('open');
        //alert();

    });
    $(document).on('click', '#show_briefcase', function () {
        getAttendeesChat(userId);
        getAttendeesDown(userId);
        getAttendeesVideos(userId);
        $('#my_briefcase').dialog('open');
    });

    $(document).on('click', '#show_talktous', function () {
        var to_user_id = 'team'; //$(this).data('to');
        var from_user_id = $(this).data('from');
        getTeamChatHistory(to_user_id, from_user_id);
        $('#talktous').dialog('open');
        getteamchat = setInterval(function () { getTeamChatHistory(to_user_id, from_user_id); }, 2000);
        //alert();
    });

    $(document).on('click', '#refresh-attendees', function () {
        var keyword = $('#attendee-search').val();

        getOnlineAttendees(keyword);
    });
    $(document).on('click', '#search-attendee', function () {
        var keyword = $('#attendee-search').val();
        //alert(keyword);
        getOnlineAttendees(keyword);
        return false;
    });
    $(document).on('click', '#clear-search-attendee', function () {
        $('#attendee-search').val('');
        getOnlineAttendees('');
        return false;
    });

    $(document).on('click', '.send_teamchat', function () {
        var sendbtn = document.querySelector('.send_teamchat');
        sendbtn.disabled = true;
        var to_user_id = 'team'; // $(this).data('to');
        var from_user_id = $(this).data('from');
        var from_src = $(this).data('src');
        var message = $('#chat_message_' + to_user_id).val();

        if (message.trim() !== '') {
            $.ajax({
                url: 'controls/server.php',
                data: { action: 'sendTeamMessage', to: to_user_id, from: from_user_id, msg: message, source: from_src },
                type: 'post',
                success: function () {
                    $('#chat_message_' + to_user_id).val('');
                }
            });
        }
        sendbtn.disabled = false;
        return false;
    });


    $(document).on('click', '.btn-chat', function () {
        //$('#attendees').dialog('close');
        var to_user_id = $(this).data('to');
        var from_user_id = $(this).data('from');
        //var to_user = getAttendeeName(to_user_id);
        var to_user;
        $.ajax({
            url: "controls/server.php",
            data: { action: 'getAttendeeName', user: to_user_id },
            type: 'post',
            dataType: "text",
            async: false
        })
            .done(function (data) {
                to_user = data;

            })
            .fail(function () {
                //console.log("error");
            })
            .always(function (data) {
                //console.log('complete');
                to_user = data;
            });
        // getChatHistory(to_user_id, from_user_id);
        var getchat = setInterval(function () { getChatHistory(to_user_id, from_user_id); }, 4000);
        //alert(to_user);
        make_chat_box(to_user_id, from_user_id);
        //$('#attendees-chat').dialog({
        $('#chat_' + to_user_id).dialog({
            dialogClass: "attendeechat",
            autoOpen: false,
            width: 400,
            height: 400,
            title: 'Chat with ' + to_user,
            position: { my: "right-100px top", at: "right bottom-100px" }

        });
        //$('#attendees-chat').dialog('open');
        $('#chat_' + to_user_id).dialog('open');

        $('#chat_' + to_user_id).dialog({
            close: function () {
                clearInterval(getchat);
            }
        });
        //alert(); 

        $(this).next('span').hide();
    });

    $(document).on('click', '.send_chat', function () {
        //$('#attendees').dialog('close');
        var sendbtn = document.querySelector('.send_chat');
        sendbtn.disabled = true;
        var to_user_id = $(this).data('to');
        var from_user_id = $(this).data('from');
        var message = $('#chat_message_' + to_user_id).val();
        if (message.trim() !== '') {
            $.ajax({
                url: 'controls/server.php',
                data: { action: 'sendMessage', to: to_user_id, from: from_user_id, msg: message },
                type: 'post',
                success: function (response) {
                    console.log(response);
                    $('#chat_message_' + to_user_id).val('');
                    //$("#attendees").html(response);
                    //getChatHistory(to_user_id, from_user_id);
                }
            });
        }
        sendbtn.disabled = false;
    });


    $(document).on('click', '.btn-email', function () {
        // alert('email');
        //$('#attendees').dialog('close');
        var to_user_id = $(this).data('to');
        var from_user_id = $(this).data('from');
        var to_user;
        $.ajax({
            url: "controls/server.php",
            data: { action: 'getAttendeeName', user: to_user_id },
            type: 'post',
            dataType: "text",
            async: false
        })
            .done(function (data) {
                // console.log("success");
                to_user = data;

            })
            .fail(function () {
                //console.log("error");
            })
            .always(function (data) {
                //console.log('complete');
                to_user = data;
            });
        make_email_box(to_user_id, from_user_id);
        //$('#attendees-chat').dialog({
        $('#email_' + to_user_id).dialog({
            autoOpen: false,
            width: 400,
            height: 300,
            title: 'Send Email to ' + to_user
        });
        //$('#attendees-chat').dialog('open');
        $('#email_' + to_user_id).dialog('open');


    });

    $(document).on('click', '.send_email', function () {
        //$('#attendees').dialog('close');
        var to_user_id = $(this).data('to');
        var from_user_id = $(this).data('from');
        var message = $('#email_message_' + to_user_id).val();

        $.ajax({
            url: 'controls/server.php',
            data: { action: 'sendEmail', to: to_user_id, from: from_user_id, msg: message },
            type: 'post',
            success: function (response) {
                if (response === 'succ') {
                    alert('Email Sent');
                    $('#email_message_' + to_user_id).val('');
                    $('#email_' + to_user_id).dialog('close');
                }
                else {
                    alert('Try again');
                }
                //$("#attendees").html(response);
                //getChatHistory(to_user_id, from_user_id);
            }
        });
    });








});

function getOnlineAttendees(keyword) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'getOnlineAttendees', key: keyword },
        type: 'post',
        success: function (response) {

            $("#attendees").html(response);

        }
    });

}

function make_chat_box(to_user_id, from_user_id) {
    var modal_content = '<div id="chat_' + to_user_id + '" class="user_dialog attendee_chat_box" title="Chat">';
    modal_content += '<div style="height:200px; border:0px solid #ccc; background-color:#ccc; overflow-y:auto; margin-bottom:15px;" class="chat_history scroll" data-touser="' + to_user_id + '" id="chat_history_' + to_user_id + '">';
    modal_content += '</div>';
    modal_content += '<div class="form-group">';
    modal_content += '<textarea name="chat_message_' + to_user_id + '" id="chat_message_' + to_user_id + '" rows="1" class="form-control" required></textarea>';
    modal_content += '</div><div class="form-group text-left">';
    modal_content += '<button type="button" name="send_chat" class="send_chat" data-to="' + to_user_id + '" data-from="' + from_user_id + '" class="btn btn-info">Send</button></div></div>';

    $('#attendees-chat').html(modal_content);


}

function make_email_box(to_user_id, from_user_id) {
    var modal_content = '<div id="email_' + to_user_id + '" class="user_dialog attendee_email_box" title="Send Email">';
    modal_content += '<div class="form-group">';
    modal_content += '<textarea name="email_message_' + to_user_id + '" id="email_message_' + to_user_id + '" rows="6" class="form-control" required></textarea>';
    modal_content += '</div><div class="form-group text-left">';
    modal_content += '<button type="button" name="send_email" class="send_email" data-to="' + to_user_id + '" data-from="' + from_user_id + '" class="btn btn-info">Send Email</button></div></div>';

    $('#attendees-email').html(modal_content);


}


function getChatHistory(to_user_id, from_user_id) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'getchathistory', to: to_user_id, from: from_user_id },
        type: 'post',
        success: function (response) {
            var tar = '#chat_history_' + to_user_id;
            var curr_hist = $(tar).html();
            if (response !== curr_hist) {
                $('#chat_history_' + to_user_id).html(response);
                var myDiv = document.getElementById('chat_history_' + to_user_id);
                myDiv.scrollTop = myDiv.scrollHeight;
            }

        }
    });
}

function checkfornewchat(to_id) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'checknewchat', to: to_id },
        type: 'post',
        success: function (response) {
            if (response > 0) {
                //alert('New Chat');
                $('#chat-message').html('<span class="badge badge-danger">' + response + '</span>').css('display', 'block');
            }
            else {
                $('#chat-message').html('').fadeOut();

            }

        }
    });
}

function getAttendeesChat(to_id) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'getAttendeesChat', to: to_id },
        type: 'post',
        success: function (response) {

            $("#attendees-list-chat").html(response);

        }
    });

}

function getAttendeesDown(to_id) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'getAttendeesDown', to: to_id },
        type: 'post',
        success: function (response) {

            $("#downloads-briefcase").html(response);

        }
    });

}

function getAttendeesVideos(to_id) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'getAttendeesVideos', to: to_id },
        type: 'post',
        success: function (response) {

            $("#videos-briefcase").html(response);

        }
    });

}



function make_exhibitor_chat_box(to_exhibitor_id, from_user_id) {
    var modal_content = '<div id="exhchat_' + to_exhibitor_id + '" class="user_dialog exhibitor_chat_box">';
    modal_content += '<div style="height:200px; border:0px solid #ccc; background-color:#ccc; overflow-y:auto; margin-bottom:15px;" class="chat_exhhistory" data-touser="' + to_exhibitor_id + '" id="chat_exhhistory_' + to_exhibitor_id + '">';
    modal_content += '</div>';
    modal_content += '<div class="form-group">';
    modal_content += '<textarea name="chat_exhmessage_' + to_exhibitor_id + '" id="chat_exhmessage_' + to_exhibitor_id + '" rows="1" class="form-control" required></textarea>';
    modal_content += '</div><div class="form-group text-left">';
    modal_content += '<button type="button" name="send_exhchat" class="send_exhchat" data-to="' + to_exhibitor_id + '" data-from="' + from_user_id + '" class="btn btn-info">Send</button></div></div>';

    $('#exhibitors-chat').html(modal_content);


}

function getExhibChatHistory(to_exhibitor_id, elem) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'getexhchathistory', to: to_exhibitor_id },
        type: 'post',
        success: function (response) {
            var tar = '#' + elem;
            //alert(tar);
            $(tar).html(response);

        }
    });
}

$(document).on('click', '.talk-title', function () {
    //$('.chat-panel').slideToggle('slow');
    //$('#talk-to-us').slideUp('slow');
    var to_exhibitor_id = $(this).data('to');
    var from_user_id = $(this).data('from');
    getExhibChatHistory(to_exhibitor_id, 'chat_history_' + to_exhibitor_id);
    var getchat = setInterval(function () { getExhibChatHistory(to_exhibitor_id, 'chat_history_' + to_exhibitor_id); }, 3000);
    $('.chat-panel').toggleClass('show');

});

$(document).on('click', '.exhsendmsg', function () {
    var sendbtn = document.querySelector('.exhsendmsg');
    sendbtn.disabled = true;
    var to_exhibitor_id = $(this).data('to');
    var from_user_id = $(this).data('from');
    var message = $('#exhchatmessage').val();
    if (message.trim() !== '') {
        $.ajax({
            url: 'controls/server.php',
            data: { action: 'sendExhMessage', to: to_exhibitor_id, from: from_user_id, msg: message },
            type: 'post',
            success: function () {
                $('#exhchatmessage').val('');
            }
        });
    }
    sendbtn.disabled = false;
    return false;
});


function showChats() {
    $('.br-tabs a').removeClass('active');
    getAttendeesChat(userId);
    $('#tab-chat').addClass('active');
    $('#briefcase-downloads').css('display', 'none');
    $('#briefcase-videos').css('display', 'none');
    $('#briefcase-inbox').css('display', 'block');
}

function showDownloads() {
    $('.br-tabs a').removeClass('active');
    getAttendeesDown(userId)
    $('#tab-dl').addClass('active');
    $('#briefcase-downloads').css('display', 'block');
    $('#briefcase-videos').css('display', 'none');
    $('#briefcase-inbox').css('display', 'none');
}

function showVideos() {
    $('.br-tabs a').removeClass('active');
    getAttendeesVideos(userId);
    $('#tab-vid').addClass('active');
    $('#briefcase-downloads').css('display', 'none');
    $('#briefcase-videos').css('display', 'block');
    $('#briefcase-inbox').css('display', 'none');
}

function getTeamChatHistory(to_user_id, from_user_id) {
    $.ajax({
        url: 'controls/server.php',
        data: { action: 'getteamchathistory', to: to_user_id, from: from_user_id },
        type: 'post',
        success: function (response) {
            var tar = '#chat_history_' + to_user_id;
            var curr_hist = $(tar).html();
            if (response !== curr_hist) {
                //console.log(response);
                //console.log(curr_hist);
                $(tar).html(response);
                var myDiv = document.getElementById('chat_history_' + to_user_id);
                myDiv.scrollTop = myDiv.scrollHeight;
            }
            else {
                //console.log('no new msg');
            }

        }
    });
}
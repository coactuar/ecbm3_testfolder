<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
<?php
echo bin2hex(random_bytes(24)); // generate unique token

?>

<!-- Add an optional button to open the popup -->
  <button class="my_briefcase_show">Open popup</button>

  <!-- Add content to the popup -->
  <div id="my_briefcase">

    ...popup content...

    <!-- Add an optional button to close the popup -->
    <button class="my_briefcase_close">Close</button>

  </div>

  <!-- Include jQuery -->
  <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>

  <!-- Include jQuery Popup Overlay -->
  <script src="js/jquery.popupoverlay.js"></script>

  <script>
    $(document).ready(function() {

      // Initialize the plugin
      $('#my_briefcase').popup();

    });
  </script>
</body>
</html>
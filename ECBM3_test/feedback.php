<?php 
require_once 'model/config.php';
require_once 'model/functions.php';
$succ = false;
$errors=[];

if (isset($_POST['fbsub-btn'])) {

    if ( empty($_POST['q01']) ||
         empty($_POST['q02']) ||
         empty($_POST['q03']) ||
         empty($_POST['q04']) ||
         empty($_POST['q05']) ||
         empty($_POST['q06']) ||
         empty($_POST['q07']) ||
         empty($_POST['q08']) ||
         empty($_POST['q09']) ||
         empty($_POST['q10']) ||
         empty($_POST['q11']) ||
         empty($_POST['q12']) ||
         empty($_POST['q13']) ||
         empty($_POST['q14']) ||
         empty($_POST['q15']) ||
         empty($_POST['q16']) ||
         empty($_POST['q17']) ||
         empty($_POST['q18']) ||
         empty($_POST['q19']) ||
         empty($_POST['q20']) ||
         empty($_POST['q21']) ||
         empty($_POST['q22']) ||
         empty($_POST['q23']) ||
         empty($_POST['q24']) ||
         empty($_POST['q25'])    
        
        ) {
        $errors['reply'] = 'Please anser all questions.';
    }
    
    if(count($errors)==0){
      $member = new User();
      $subFeedback = $member->submitFeedback();

      if($subFeedback['status']=='success')
      {
          $succ = true;
      }
      else{
        $errors['msg'] = $subFeedback['message'];
      }
    }


}



?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $event_title; ?></title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/all.min.css">
<link rel="stylesheet" href="css/styles.css">

</head>

<body>

	<div class="container">
        <div class="row no-margin">
            <div class="col-12">
                <img src="img/reg-banner.jpg" class="img-fluid" alt=""/> 
            </div>
        </div>
        <div class="row bg-white color-grey">
            <div class="col-12 text-center">
                 <h3 class="reg-title">Feedback Form</h3>
            </div>
        </div>
        <div class="row bg-white color-grey">
            <div class="col-12 col-md-8 offset-md-2">
                <?php if (!$succ) { ?>
                <div id="register-area">
                  <?php
                      if (count($errors) > 0): ?>
                      <div class="alert alert-danger">
                        <ul class="list-unstyled">
                        <?php foreach ($errors as $error): ?>
                        <li>
                          <?php echo $error; ?>
                        </li>
                        <?php endforeach;?>
                        </ul>
                      </div>
                    <?php endif;
                    ?>
                  <form method="POST">
                      <input type="hidden" id="name" name="name" class="input" value="<?php echo $_SESSION['user_first_name']. ' '.$_SESSION['user_last_name']; ?>" autocomplete="off" required>
                      <input type="hidden" id="emailid" name="emailid" class="input" value="<?php echo $_SESSION['user_emailid']; ?>" autocomplete="off" required>
                      
                      <div class="row mb-1">
                        <div class="col-12">
                            <strong>Thank you for attending the OTA Best of Orthopaedic Techniques Live 2020. Please let us know about your experience in the course and feedback for the betterment of future programs.</strong>
                        </div>
                      </div>
                      <div class="row mt-3 mb-1">
                          <div class="col-12">
                              <table class="table">
                                <tr align="center">
                                    <th align="left">Please rate the following</td>
                                    <th width="150">Strongly Agree</td>
                                    <th width="150">Strongly Disagree</td>
                                    <th width="100">Agree</td>
                                    <th width="100">Disagree</td>
                                </tr>
                                <tr align="center">
                                    <td align="left">The content will be useful in my practice.</td>
                                    <td><input type="radio" name="q01" value="Strongly Agree"></td>
                                    <td><input type="radio" name="q01" value="Strongly Disagree"></td>
                                    <td><input type="radio" name="q01" value="Agree"></td>
                                    <td><input type="radio" name="q01" value="Disagree"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Appropriate time was allocated for discussion.</td>
                                    <td><input type="radio" name="q02" value="Strongly Agree"></td>
                                    <td><input type="radio" name="q02" value="Strongly Disagree"></td>
                                    <td><input type="radio" name="q02" value="Agree"></td>
                                    <td><input type="radio" name="q02" value="Disagree"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">The educational design/format supported my learning.</td>
                                    <td><input type="radio" name="q03" value="Strongly Agree"></td>
                                    <td><input type="radio" name="q03" value="Strongly Disagree"></td>
                                    <td><input type="radio" name="q03" value="Agree"></td>
                                    <td><input type="radio" name="q03" value="Disagree"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">I would recommend this course to a colleague.</td>
                                    <td><input type="radio" name="q04" value="Strongly Agree"></td>
                                    <td><input type="radio" name="q04" value="Strongly Disagree"></td>
                                    <td><input type="radio" name="q04" value="Agree"></td>
                                    <td><input type="radio" name="q04" value="Disagree"></td>
                                </tr>
                              </table>
                          </div>
                      </div>
                      <div class="row mb-1">
                        <div class="col-12">
                            <strong>Please rate the faculty's quality of teaching.</strong>
                        </div>
                      </div>
                      <div class="row mt-3 mb-1">
                          <div class="col-12">
                              <table class="table">
                                <tr align="center">
                                    <th align="left">Faculty</td>
                                    <th width="150">Excellent</td>
                                    <th width="100">Good</td>
                                    <th width="100">Fair</td>
                                    <th width="150">Poor</td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Jesse Jupiter</td>
                                    <td><input type="radio" name="q05" value="Excellent"></td>
                                    <td><input type="radio" name="q05" value="Good"></td>
                                    <td><input type="radio" name="q05" value="Fair"></td>
                                    <td><input type="radio" name="q05" value="Poor"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Lisa Cannada</td>
                                    <td><input type="radio" name="q06" value="Excellent"></td>
                                    <td><input type="radio" name="q06" value="Good"></td>
                                    <td><input type="radio" name="q06" value="Fair"></td>
                                    <td><input type="radio" name="q06" value="Poor"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Hassan Mir</td>
                                    <td><input type="radio" name="q07" value="Excellent"></td>
                                    <td><input type="radio" name="q07" value="Good"></td>
                                    <td><input type="radio" name="q07" value="Fair"></td>
                                    <td><input type="radio" name="q07" value="Poor"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Paul Tornetta</td>
                                    <td><input type="radio" name="q08" value="Excellent"></td>
                                    <td><input type="radio" name="q08" value="Good"></td>
                                    <td><input type="radio" name="q08" value="Fair"></td>
                                    <td><input type="radio" name="q08" value="Poor"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Samir Mehta</td>
                                    <td><input type="radio" name="q09" value="Excellent"></td>
                                    <td><input type="radio" name="q09" value="Good"></td>
                                    <td><input type="radio" name="q09" value="Fair"></td>
                                    <td><input type="radio" name="q09" value="Poor"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Richard Bransford</td>
                                    <td><input type="radio" name="q10" value="Excellent"></td>
                                    <td><input type="radio" name="q10" value="Good"></td>
                                    <td><input type="radio" name="q10" value="Fair"></td>
                                    <td><input type="radio" name="q10" value="Poor"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Doug Lundy</td>
                                    <td><input type="radio" name="q11" value="Excellent"></td>
                                    <td><input type="radio" name="q11" value="Good"></td>
                                    <td><input type="radio" name="q11" value="Fair"></td>
                                    <td><input type="radio" name="q11" value="Poor"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Milan Sen</td>
                                    <td><input type="radio" name="q12" value="Excellent"></td>
                                    <td><input type="radio" name="q12" value="Good"></td>
                                    <td><input type="radio" name="q12" value="Fair"></td>
                                    <td><input type="radio" name="q12" value="Poor"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Bill Ombresky</td>
                                    <td><input type="radio" name="q13" value="Excellent"></td>
                                    <td><input type="radio" name="q13" value="Good"></td>
                                    <td><input type="radio" name="q13" value="Fair"></td>
                                    <td><input type="radio" name="q13" value="Poor"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Aaron Nauth</td>
                                    <td><input type="radio" name="q14" value="Excellent"></td>
                                    <td><input type="radio" name="q14" value="Good"></td>
                                    <td><input type="radio" name="q14" value="Fair"></td>
                                    <td><input type="radio" name="q14" value="Poor"></td>
                                </tr>
                              </table>
                          </div>
                      </div>
                      
                      <div class="row mb-1">
                        <div class="col-12">
                            <strong>How would you rate the content you obtained for each of the broad topic?</strong>
                        </div>
                      </div>
                      <div class="row mt-3 mb-1">
                          <div class="col-12">
                              <table class="table">
                                <tr align="center">
                                    <th align="left">Broad Topic</td>
                                    <th width="200">Exceptional content with new learnings </td>
                                    <th width="200">Ordinary content with few learnings </td>
                                    <th width="150">Needs Improvement </td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Upper Extremity Trauma</td>
                                    <td><input type="radio" name="q15" value="Exceptional"></td>
                                    <td><input type="radio" name="q15" value="Ordinary"></td>
                                    <td><input type="radio" name="q15" value="Needs Improvement"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Pelvic Trauma & Hip Fractures</td>
                                    <td><input type="radio" name="q16" value="Exceptional"></td>
                                    <td><input type="radio" name="q16" value="Ordinary"></td>
                                    <td><input type="radio" name="q16" value="Needs Improvement"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Foot and Ankle Injuries</td>
                                    <td><input type="radio" name="q17" value="Exceptional"></td>
                                    <td><input type="radio" name="q17" value="Ordinary"></td>
                                    <td><input type="radio" name="q17" value="Needs Improvement"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Spine Trauma</td>
                                    <td><input type="radio" name="q18" value="Exceptional"></td>
                                    <td><input type="radio" name="q18" value="Ordinary"></td>
                                    <td><input type="radio" name="q18" value="Needs Improvement"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Knee Trauma</td>
                                    <td><input type="radio" name="q19" value="Exceptional"></td>
                                    <td><input type="radio" name="q19" value="Ordinary"></td>
                                    <td><input type="radio" name="q19" value="Needs Improvement"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Sports Injuries; Arthroscopic Procedures</td>
                                    <td><input type="radio" name="q20" value="Exceptional"></td>
                                    <td><input type="radio" name="q20" value="Ordinary"></td>
                                    <td><input type="radio" name="q20" value="Needs Improvement"></td>
                                </tr>
                                <tr align="center">
                                    <td align="left">Interesting Case Presentations</td>
                                    <td><input type="radio" name="q21" value="Exceptional"></td>
                                    <td><input type="radio" name="q21" value="Ordinary"></td>
                                    <td><input type="radio" name="q21" value="Needs Improvement"></td>
                                </tr>
                                
                              </table>
                          </div>
                      </div>
                      <div class="row mb-1">
                        <div class="col-12">
                            <strong>Describe your level of practice in orthopaedics:</strong>
                        </div>
                      </div>
                      <div class="row mb-1">
                        <div class="col-12">
                            <ul class="list-unstyled">
                                <li><input type="radio" name="q22" value="Resident/In-training"> Resident/In-training</li>
                                <li><input type="radio" name="q22" value="0-5 years of practice"> 0-5 years of practice</li>
                                <li><input type="radio" name="q22" value="5-10 years of practice"> 5-10 years of practice</li>
                                <li><input type="radio" name="q22" value="10-20 years of practice"> 10-20 years of practice</li>
                                <li><input type="radio" name="q22" value="20+ years of practice"> 20+ years of practice</li>
                            </ul>
                        </div>
                      </div>
                      <div class="row mb-1">
                        <div class="col-12">
                            <strong>What is your primary specialty area?</strong>
                        </div>
                      </div>
                      <div class="row mb-1">
                        <div class="col-12">
                            <ul class="list-unstyled">
                                <li><input type="radio" name="q23" value="Generalist"> Generalist</li>
                                <li><input type="radio" name="q23" value="Shoulder and Elbow"> Shoulder and Elbow</li>
                                <li><input type="radio" name="q23" value="Spine"> Spine</li>
                                <li><input type="radio" name="q23" value="Sports Medicine"> Sports Medicine</li>
                                <li><input type="radio" name="q23" value="Pediatrics"> Pediatrics</li>
                                <li><input type="radio" name="q23" value="Hip and Knee"> Hip and Knee</li>
                                <li><input type="radio" name="q23" value="Foot and Ankle"> Foot and Ankle</li>
                                <li><input type="radio" name="q23" value="Trauma"> Trauma</li>
                                <li><input type="radio" name="q23" value="Tumors"> Tumors</li>
                            </ul>
                        </div>
                      </div>
                      <div class="row mb-2">
                        <div class="col-12">
                            <strong>Which topic or faculty would you like to hear in the next program?</strong>
                            <br>
                            <textarea name="q24" id="q24" rows="4" class="input"></textarea>
                        </div>
                      </div>
                      <div class="row mb-2">
                        <div class="col-12">
                            <strong>How would you rate this virtual form of education engagement? (1 – lowest 5 – Highest)</strong>
                            <ul class="list-unstyled">
                                <li><input type="radio" name="q25" value="1"> 1</li>
                                <li><input type="radio" name="q25" value="2"> 2</li>
                                <li><input type="radio" name="q25" value="3"> 3</li>
                                <li><input type="radio" name="q25" value="4"> 4</li>
                                <li><input type="radio" name="q25" value="5"> 5</li>
                                
                            </ul>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <input type="submit" name="fbsub-btn" id="btnSubmit" class="btn btn-submit" value="">
                      </div>
                      
                      
                      
                  </form>
                </div>
                <?php } else { ?>
                  <div id="registration-confirmation">
                      <div class="alert alert-success">
                      Thanks for giving us your valuable feedback!<br>
                      </div>
                      
                  </div>
                <?php } ?>
                 
            </div>
        </div>
        <div class="row bg-white">
            <div class="col-12">
                <img src="img/line-h.jpg" class="img-fluid" alt=""/> 
            </div>
        </div>
        <div class="row bg-white p-2">
            <div class="col-4 bor-right p-2 text-center">
                <img src="img/in-assoc.png" class="img-fluid bot-img" alt=""/>
            </div>
            <div class="col-4 p-2 text-center">
                <img src="img/sci-partner.png" class="img-fluid bot-img" alt=""/>
            </div>
          <div class="col-4 bor-left p-2 text-center color-grey">
                <img src="img/brought-by.png" class="img-fluid bot-img" alt=""/>
                <div class="visit">
                Visit us at <a href="https://www.integracehealth.com/about.html" class="link" target="_blank">https://www.integracehealth.com/about.html</a>
                </div>
          </div>
        </div>
        
        
	</div>
<div id="code">IPL/O/WI/14072020</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-20"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-20');
</script>

</body>
</html>
<?php 
require_once 'model/config.php';
require_once 'model/functions.php';

$fname ='';
$lname ='';
$email ='';
$phone ='';
$state = '0';
$city = '';
$division ='';

$errors=[];
$succ = false;

if (isset($_POST['reg-btn'])) {
    if (empty($_POST['fname'])) {
        $errors['fname'] = 'First Name required';
    }
    
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['emailid'];
    $phone = $_POST['phone'];
    $division = $_POST['division'];
    $state = $_POST['state'];
    $city = $_POST['city'];
    
    if(strlen($phone) != 10)
    {
        $errors['phone_len'] = 'Phone No. must be 10 digits.';
    }
    
    if($division == '0'){
        $errors['division'] = 'Select your division';
    }
    if($state == '0'){
        $errors['state'] = 'Select your state';
    }
    
    if(count($errors)==0){
      $member = new User();
      $registrationResponse = $member->registerMember();
      //var_dump($registrationResponse);
      if($registrationResponse['status']=='success')
      {
          $succ = true;
      }
      else{
        $errors['msg'] = $registrationResponse['message'];
      }
    }
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $event_title; ?></title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/all.min.css">
<link rel="stylesheet" href="css/styles.css">

</head>

<body>

	<div class="container">
        
        <div class="row mt-1 bg-white color-grey">
            <div class="col-12 text-center">
                 <h3 class="reg-title">Register for Integrace e-CBM2!</h3>
            </div>
        </div>
        <div class="row bg-white color-grey">
            <div class="col-12 col-md-8 offset-md-2">
                <?php if (!$succ) { ?>
                <div id="register-area">
                  <?php
                      if (count($errors) > 0): ?>
                      <div class="alert alert-danger">
                        <ul class="list-unstyled">
                        <?php foreach ($errors as $error): ?>
                        <li>
                          <?php echo $error; ?>
                        </li>
                        <?php endforeach;?>
                        </ul>
                      </div>
                    <?php endif;
                    ?>
                  <form method="POST">
                      <div class="row mt-2">
                          <div class="col-12 col-md-6">
                              <label>First Name<sup class="req">*</sup></label>
                              <input type="text" id="fname" name="fname" class="input" value="<?php echo $fname; ?>" autocomplete="off" required>
                          </div>
                          <div class="col-12 col-md-6">
                              <label>Last Name<sup class="req">*</sup></label>
                              <input type="text" id="lname" name="lname" class="input" value="<?php echo $lname; ?>" autocomplete="off" required>
                          </div>
                      </div>
                      <div class="row mt-3 mb-1">
                          <div class="col-12">
                              <label>Email ID<sup class="req">*</sup></label>
                              <input type="email" id="emailid" name="emailid" class="input" value="<?php echo $email; ?>" autocomplete="off" required>
                          </div>
                      </div>
                      <div class="row mt-3 mb-1">
                          <div class="col-12">
                              <label>Phone No.<sup class="req">*</sup></label>
                              <input type="number" id="phone" name="phone" class="input" value="<?php echo $phone; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-12">
                              <label class="mt-2">Select Division?<sup class="req">*</sup></label>
                              <select id="division" name="division" class="input mb-2" required>
                                <option value="0">Select Division</option>
                                <option value="Head Office" <?php if($division=='Head Office') echo 'selected'; ?>>Head Office</option>
                                <option value="Orthopaedics" <?php if($division=='Orthopaedics') echo 'selected'; ?>>Orthopaedics</option>
                                <option value="Gynaecology" <?php if($division=='Gynaecology') echo 'selected'; ?>>Gynaecology</option>
                              </select>
                          </div>
                      </div>
                      <div class="row mt-3 mb-1">
                          <div class="col-12 col-md-6">
                              <label>State<sup class="req">*</sup></label>
                              <div id="states">
                              <select class="input" id="state" name="state" required>
                                  <option>Select State</option>
                              </select>
                              </div>
                          </div>
                          <div class="col-12 col-md-6">
                              <label>City<sup class="req">*</sup></label>
                              <input type="text" id="city" name="city" class="input" value="<?php echo $city; ?>" autocomplete="off" required>
                          </div>
                      </div>
                      
                      
                      <div class="row mt-2 mb-3">
                          <div class="col-12">
                              <small><sup class="req">*</sup> denotes mandatory fields.</small><br><br>
                              <input type="submit" name="reg-btn" id="btnRegister" class="btn btn-register" value="">
                              <a href="./" class="form-cancel"><img src="img/cancel-btn.jpg" alt=""/></a>
                          </div>
                      
                      </div>
                  </form>
                </div>
                <?php } else { ?>
                  <div id="registration-confirmation">
                      <div class="alert alert-success">
                      You are registered succesfully!</b><br>
                      </div>
                      
                  <a href="./">Continue to Login</a> 
                  </div>
                <?php } ?>
                 
            </div>
        </div>
        <div class="row bg-white">
            <div class="col-12">
                <img src="img/line-h.jpg" class="img-fluid" alt=""/> 
            </div>
        </div>
        <div class="row bg-white p-2">
            
            
          <div class="col-12 p-2 text-center color-grey">
                <img src="img/brought-by.png" class="img-fluid bot-img" alt=""/>
                <div class="visit">
                Visit us at <a href="https://www.integracehealth.com/about.html" class="link" target="_blank">https://www.integracehealth.com/about.html</a>
                </div>
          </div>
        </div>
        
        
	</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
  function updateState()
  {
      var c = 101;
      if(c!='0'){
          $.ajax({
              url: 'controls/server.php',
              data: {action: 'getstates', country : c },
              type: 'post',
              success: function(response) {
                  
                  $("#states").html(response);
              }
          });
      }
  }
  updateState();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-22"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-22');
</script>


</body>
</html>